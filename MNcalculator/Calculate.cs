﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MNcalculator
{
    public class Calculate
    {
        /// <summary>
        /// This method converts string to the list with digits and chars after validation
        /// </summary>
        /// <param name="expression">Expression after valid and remove space</param>
        /// <returns></returns>
        public static List<string> ListWithExpression(string expression)
        {
            //The list with numbers and chars separated
            List<string> ValueList = new List<string>();

            //The pattern for numbers
            string patternForDigits = @"(\d+)";
            //The pattern for chars
            string paternForChars = @"(\*+)|(\++)|(\-+)|(\/+)";

            while (true)
            {
                //Find the first digit on the left side
                Match match = Regex.Match(expression, patternForDigits);

                //if you found digit
                if (match.Success)
                {
                    //Add to the list
                    ValueList.Add(match.Value);
                }


                //Check if there is any number (the chars can not be last)
                if (1 < expression.Length)
                {
                    //Remove the found number from the expression
                    expression = expression.Substring(match.Value.Length);

                    //If the expression is empty, do not look for the chars
                    if (expression.Length != 0)
                    {
                        //Find the first char on the left side
                        Match match2 = Regex.Match(expression, paternForChars);

                        //if you found char
                        if (match2.Success)
                        {
                            //Add to the list
                            ValueList.Add(match2.Value);
                        }

                        //Remove the found char from the expression
                        expression = expression.Substring(match2.Value.Length);
                    }
                }
                else
                {
                    //In this list the numbers and chars are in separate fields
                    return ValueList;
                }
            }
        }


        /// <summary>
        /// This method calculates your expression based on the list with digits and chars
        /// </summary>
        /// <param name="ListWithExpression">List with digits and chars</param>
        /// <returns></returns>
        public static double CalculateMethod(List<string> ListWithExpression)
        {
            //The "regex" pattern for multiplication and division
            string paternForMulAndDiv = @"(\*+)|(\/+)";

            //Convert the list to array 
            var arrayMulAndDiv = ListWithExpression.ToArray();
            for (int i = 0; i < arrayMulAndDiv.Length; i++)
            {

                //Find the first multiplication or division on the left side
                Match match2 = Regex.Match(arrayMulAndDiv[i], paternForMulAndDiv);

                if (match2.Success)
                {
                    //if you found multiplication
                    if (match2.Value == "*")
                    {
                        //Convert the value by 1 smaller and by 1 bigger than the index of the chars and multiply them
                        var resoult = Convert.ToDouble(arrayMulAndDiv[i - 1]) * Convert.ToDouble(arrayMulAndDiv[i + 1]);
                        //Save the result in the smallest index place on which you used 
                        arrayMulAndDiv[i - 1] = Convert.ToString(resoult);
                        //Convert to the list to remove 2 empties index (It's hard to remove items from array, you have to resize array to do this, conversion is simpler method)
                        var auxList = arrayMulAndDiv.ToList();
                        //Remove empty elements
                        auxList.RemoveAt(i);
                        auxList.RemoveAt(i);

                        //Convert list to array and check array one more time 
                        arrayMulAndDiv = auxList.ToArray();
                        //Break the loop and reset the loop counter because if we did not do it, we would not keep the sequence of expression
                        i = 0;
                        continue;
                    }

                    if (match2.Value == "/")
                    {
                        //Convert the value by 1 smaller and by 1 bigger than the index of the chars and division them
                        var resoult = Convert.ToDouble(arrayMulAndDiv[i - 1]) / Convert.ToDouble(arrayMulAndDiv[i + 1]);
                        //Save the result in the smallest index place on which you used 
                        arrayMulAndDiv[i - 1] = Convert.ToString(resoult);
                        //Convert to list
                        var auxList = arrayMulAndDiv.ToList();
                        //remove 2 empties index
                        auxList.RemoveAt(i);
                        auxList.RemoveAt(i);
                        //Convert the list to array
                        arrayMulAndDiv = auxList.ToArray();
                        //Break the loop and reset the loop counter
                        i = 0;
                        continue;
                    }
                }
            }

            //The "regex" pattern for addition and subtraction
            string paternForAddSub = @"(\++)|(\-+)";
            //Convert the list to array 
            var arrayAddAndSub = arrayMulAndDiv;
            for (int i = 0; i < arrayAddAndSub.Length; i++)
            {
                //Find the first addition or subtraction on the left side
                Match match2 = Regex.Match(arrayAddAndSub[i], paternForAddSub);
                if (match2.Success)
                {
                    //If you found addition
                    if (match2.Value == "+")
                    {
                        //Convert the value by 1 smaller and by 1 bigger than the index of the chars and addition them
                        var resoult = Convert.ToDouble(arrayAddAndSub[i - 1]) + Convert.ToDouble(arrayAddAndSub[i + 1]);
                        //Save the result in the smallest index place on which you used 
                        arrayAddAndSub[i - 1] = Convert.ToString(resoult);
                        //Convert to list
                        var helplist = arrayAddAndSub.ToList();
                        //remove 2 empties index
                        helplist.RemoveAt(i);
                        helplist.RemoveAt(i);
                        //Convert the list to array
                        arrayAddAndSub = helplist.ToArray();
                        //Break the loop and reset the loop counter
                        i = 0;
                        continue;
                    }

                    //If you found subtraction
                    if (match2.Value == "-")
                    {
                        //Convert the value by 1 smaller and by 1 bigger than the index of the chars and subtract them
                        var resoult = Convert.ToDouble(arrayAddAndSub[i - 1]) - Convert.ToDouble(arrayAddAndSub[i + 1]);
                        //Save the result in the smallest index place
                        arrayAddAndSub[i - 1] = Convert.ToString(resoult);
                        //Convert to list
                        var helplist = arrayAddAndSub.ToList();
                        //remove 2 empties index
                        helplist.RemoveAt(i);
                        helplist.RemoveAt(i);
                        //Convert the list to array
                        arrayAddAndSub = helplist.ToArray();
                        //Break the loop and reset the loop counter
                        i = 0;
                        continue;
                    }
                }
            }
            //Return resoult expression
            double resoultExpression = Convert.ToDouble(arrayAddAndSub[0]);
            return resoultExpression;

        }
    }
}
