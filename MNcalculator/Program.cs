﻿using System;

namespace MNcalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                //Display message
                Console.WriteLine("Enter the expression to solve, remember, you can only use digits and the following characters +, -, *, /");
                //Take the expression
                string userExpression = Console.ReadLine();
                //Remove all space
                userExpression = StringValidator.RemoveSpace(userExpression);
                //If the expression is correct
                if (StringValidator.IsValid(userExpression))
                {
                    //Call this method to get a list with separate digits and chars
                    var auxList = Calculate.ListWithExpression(userExpression);
                    //Calculate the result of the expression based on the list of expressions
                    Console.WriteLine(Calculate.CalculateMethod(auxList));
                }
            }
        }
    }
}
