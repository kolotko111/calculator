﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MNcalculator
{
    public class StringValidator
    {
        /// <summary>
        /// This method validates string with expression from user
        /// </summary>
        /// <param name="expression">Expression to valid from user</param>
        /// <returns></returns>
        public static bool IsValid(string expression)
        {
            //Check if the field with the expression is not empty
            if (String.IsNullOrEmpty(expression))
            {
                //If empty, return false
                return false;
            }

            //Check the correctness of chars
            //Check each chars individually and check if it is correct
            //[0-9] - value between 0-9 or
            //\+ - is it + or
            //\- is it - or
            //\* is it - * or
            //\/ is it - /
            string patern = @"([0-9]|\+|\-|\*|\/)";
            foreach (var item in expression)
            {
                //Checking each chars
                Match match = Regex.Match(item.ToString(), patern);
                //if the chars is not correct return false
                if (!match.Success)
                {
                    return false;
                }
            }

            //String is a chars array. Check if there is no char in the last place
            string paternForLastIndex = @"(\+|\-|\*|\/)";
            Match matchForLastIndex = Regex.Match(expression[expression.Length - 1].ToString(), paternForLastIndex);
            if (matchForLastIndex.Success)
            {
                return false;
            }

            //Check if not 2 chars are next to each other. If 2 chars are in the last place there would be an exception, but the previous test protects us against this
            string paternForDoubleChars = @"(\+|\-|\*|\/)";
            for (int i = 0; i < expression.Length; i++)
            {
                //Checking each chars
                Match match = Regex.Match(expression[i].ToString(), paternForDoubleChars);
                //if you found chars
                if (match.Success)
                {
                    //Check if there is no char in the next place
                    Match match2 = Regex.Match(expression[i + 1].ToString(), paternForDoubleChars);
                    //If is
                    if (match2.Success)
                    {
                        return false;
                    }
                }
            }


            //If everything is ok, return true
            return true;
        }

        /// <summary>
        /// This method remove all space in string
        /// </summary>
        /// <param name="expression">Expression from user</param>
        /// <returns></returns>
        public static string RemoveSpace(string expression)
        {
            //Remove all space from string
            expression = expression.Replace(" ", "");

            //Return expression without space
            return expression;
        }
    }
}
