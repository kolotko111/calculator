﻿using MNcalculator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MNCalculator.Test
{
    class StringValidatorTest
    {
        //Expression is empty
        [TestCase("", false)]
        //Forbidden char
        [TestCase("1+2+3a", false)]
        //Forbidden char
        [TestCase("1+2b+3", false)]
        //Forbidden char
        [TestCase("1c+2+3", false)]
        //Char on last place
        [TestCase("1+2+3+", false)]
        //Double++
        [TestCase("1++2+3", false)]
        //Double--
        [TestCase("1+2--3", false)]
        //Double**
        [TestCase("1**2-3", false)]
        //Double//
        [TestCase("1*2//3", false)]
        //Everything ok
        [TestCase("1*2+3-4/5", true)]
        public void IsValidTest(string testExpression, bool testResoult)
        {
            //Act
            bool resoult = StringValidator.IsValid(testExpression);
            //Assert
            Assert.AreEqual(testResoult, resoult);
        }

        //Space at the beginning expression
        [TestCase("   1+2", "1+2")]
        //Space at the beginning and at the middle expression
        [TestCase("   1+    2", "1+2")]
        //Space at the beginning, at the middle and at the end expression
        [TestCase("   1+    2    ", "1+2")]
        //Space at the end expression
        [TestCase("1+2   ", "1+2")]
        public static void RemoveSpaceTest(string testExpression, string testResoult)
        {
            //Act
            var returnExpression = StringValidator.RemoveSpace(testExpression);
            //Assert
            Assert.AreEqual(testResoult, returnExpression);
        }
    }
}
