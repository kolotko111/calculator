﻿using MNcalculator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MNCalculator.Test
{
    class CalculateTest
    {
        //First expresiom from "Zadanie-Microsoft-1"
        [TestCase("4+5*2", 14)]
        //Second expresiom from "Zadanie-Microsoft-1"
        [TestCase("4+5/2", 6.5)]
        //Third expresiom from "Zadanie-Microsoft-1"
        [TestCase("4+5/2-1", 5.5)]
        public void IsValidTest(string testExpression, double testResoult)
        {
            //Arange
            //Call this method to get a list with separate digits and characters
            var auxList = Calculate.ListWithExpression(testExpression);

            //Act
            //Calculate the result of the expression based on the list of expressions
            var returnValueFromMethod = Calculate.CalculateMethod(auxList);

            //Assert
            Assert.AreEqual(testResoult, returnValueFromMethod);
        }
    }
}
